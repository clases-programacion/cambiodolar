package cambiodolar;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase para leer un csv.
 */
class CSVReader {
    //private String archivoCsv = "./Historicos_USD_MXN.csv";
    /**
     * Nombre del archivo csv.
     */
    private String archivoCsv = "Mensual_Historico_USD_MXN.csv";
    //private String archivoCsv = "Semanal_Historico_USD_MXN.csv";

    /**
     * Lee el archivo csv y lo almacena en una lista de arrays.
     *
     * @return
     */
    public List<String[]> getCsv() {
        String csvFile = this.archivoCsv;
        BufferedReader br = null;
        String line = "";
        //Separador de columnas
        String cvsSplitBy = ",";
        List<String[]> precioDolarHistorico = new ArrayList<>();
        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String cline[] = line.split(cvsSplitBy);
                precioDolarHistorico.add(cline);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return precioDolarHistorico;
    }
}

