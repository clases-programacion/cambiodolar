package cambiodolar;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import javax.swing.*;
import java.util.List;

public class Graficar extends JFrame {
    /**
     * Constructor.
     *
     * @param applicationTitle
     * @param chartTitle
     */
    public Graficar(String applicationTitle, String chartTitle) {
        super(applicationTitle);
        JFreeChart chart = ChartFactory.createAreaChart(chartTitle, "Categorias", "Precio", createDataset(),
                PlotOrientation.VERTICAL, true, true, true);
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(800, 620));
        setContentPane(chartPanel);
    }

    /**
     * Añade datos a la gráfica.
     * 
     * @return
     */
    private CategoryDataset createDataset() {
        CSVReader precioDolar = new CSVReader();
        List<String[]> precios = precioDolar.getCsv();
        final String maximo = "Máximo";
        final String minimo = "Mínimo";
        final String variacion = "Variación";
        final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (String[] precio : precios) {
            //dataset.addValue(Double.parseDouble(precio[1]), cierre, precio[0]);
            //dataset.addValue(Double.parseDouble(precio[2]), apertura, precio[0]);
            dataset.addValue(Double.parseDouble(precio[3]), maximo, precio[0]);
            dataset.addValue(Double.parseDouble(precio[4]), minimo, precio[0]);
            dataset.addValue(Double.parseDouble(precio[5]), variacion, precio[0]);
            System.out.println(precio[0].toString());
        }
        return dataset;
    }
}
